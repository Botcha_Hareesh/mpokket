*** Settings ***
Resource          ../../Config/super.robot

*** Keywords ***
Launch Mobile Application
    [Arguments]    ${platform_name}
    [Documentation]    Launch Application in Android and iOS platforms.
    ...
    ...    Examples:
    ...    mobile_common.Launch Mobile Application ${PLATFORM_NAME}
    ...    mobile_common.Launch Mobile Application Android/ios
    Run Keyword If    '${platform_name}'=='Android'    AppiumLibrary.Open Application    ${APPIUM_SERVER_URL}    platformName=Android    platformVersion=${ANDROID_PLATFORM_VERSION}    deviceName=${ANDROID_DEVICE_NAME}    app=${ANDROID_APP}    automationName=${ANDROID_AUTOMATION_NAME}    noReset=${ANDROID_NO_RESET_APP}    autoAcceptAlerts=True    autoGrantPermissions=True    newCommandTimeout=${NEW_COMMAND_TIMEOUT}
    Run Keyword If    '${platform_name}'=='iOS'    AppiumLibrary.Open Application    ${APPIUM_SERVER_URL}    platformName=iOS    platformVersion=${IOS_PLATFORM_VERSION}    deviceName=${IOS_DEVICE_NAME}    app=${IOS_APP}    udid=${UDID}    bundleId=${BUNDLE_ID}    automationName=${IOS_AUTOMATION_NAME}    noReset=${IOS_NO_RESET_APP}    autoAcceptAlerts=True
    Run Keyword If    '${platform_name}'!='iOS' and '${platform_name}'!='Android'    Fail    Platform Name used '${platform_name}'. Please provide valid Platform Name.
    Set Appium Timeout    15s

Input Text
    [Arguments]    ${locator}    ${data}
    [Documentation]    Enters Text into textbox, and Hide the android keyboard.
    ...
    ...    Example:
    ...    mobile_common.Input Text locator text
    Run Keyword If    '${data}'!='NA'    AppiumLibrary.Input Text    ${locator}    ${data}
    Comment    AppiumLibrary.Hide Keyboard

Read TestData From Excel
    [Arguments]    ${testcaseid}    ${sheet_name}
    [Documentation]    Read TestData from excel file for required data.
    ...
    ...    Example:
    ...    Read TestData From Excel TC_01 SheetName
    ${test_prerequisite_data}    CustomLibrary.Get Ms Excel Row Values Into Dictionary Based On Key    ${TESTDATA_FOLDER}/TestData.xlsx    ${testcaseid}    ${sheet_name}
    Set Global Variable    ${test_prerequisite_data}
    [Return]    ${test_prerequisite_data}

Fail and take screenshot
    [Arguments]    ${message}
    AppiumLibrary.Capture Page Screenshot
    Fail    ${message}

Update Dynamic Value
    [Arguments]    ${locator}    ${value}
    [Documentation]    It replace the string where ever you want.
    ...
    ...    Example:
    ...    mobile_common.Update Dynamic Value locator replace_string
    ${xpath}=    Replace String    ${locator}    replaceText    ${value}
    [Return]    ${xpath}

Print Languages from App and Select Language
    [Arguments]    ${count}    ${expected_langauge}
    AppiumLibrary.Wait Until Element Is Visible    ${label.welcome_page.languages}    ${MEDIUM_WAIT}    Languages page is not visible
    FOR    ${value}    IN RANGE    1    ${count}+1
        ${value}    Convert to string    ${value}
        ${label.welcome_page.language.new}    Update Dynamic Value    ${label.welcome_page.language}    ${value}
        AppiumLibrary.Wait Until Element Is Visible    ${label.welcome_page.language.new}    ${MEDIUM_WAIT}
        ${app_language}    AppiumLibrary.Get text    ${label.welcome_page.language.new}
        Log    ${app_language}
        Run keyword if    '${app_language}'=='${expected_langauge}'    AppiumLibrary.Click Element    ${label.welcome_page.language.new}
    END
    [Return]    ${app_language}

Navigate to enter Email to login
    xpath=(//android.widget.TextView[@resource-id='com.flipkart.android:id/tv_text'])[6]    ${LONG_WAIT}
    xpath=(//android.widget.TextView[@resource-id='com.flipkart.android:id/tv_text'])[6]
    ${button.continue}    ${MEDIUM_WAIT}    Contoinue button is not visible
    ${button.continue}
    //android.widget.Button[@resource-id='com.google.android.gms:id/cancel']    ${LONG_WAIT}
    //android.widget.Button[@resource-id='com.google.android.gms:id/cancel']
    AppiumLibrary.Wait Until Element Is Visible    ${textbox.login.email}    ${LONG_WAIT}
    3s

Login to app
    [Arguments]    ${Username}    ${Password}
    Comment    mobile_common.Cancel Update Version Message
    mobile_common.Enter Login Details    ${Username}    ${Password}
    ${status}    Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    //android.widget.TextView[@resource-id='com.flipkart.android:id/search_widget_textbox']    ${LONG_WAIT}    Schemas screen is not displayed after waiting ${LONG_WAIT} seconds
    Run Keyword If    ${status} == False    mobile_common.Fail and take screenshot    Unable to login to application with valid credentials ${Username} and ${Password}

Navigate to MyAccount
    [Arguments]    ${email_ID}
    AppiumLibrary.Wait Until Element Is Visible    ${button.menu}    ${LONG_WAIT}
    AppiumLibrary.Click Element    ${button.menu}
    Sleep    3s
    Comment    AppiumLibrary.Wait Until Element Is Visible    //android.widget.TextView[@text='My Account']    ${LONG_WAIT}
    AppiumLibrary.Click Element    ${button.My_Account}
    ${label.user_email_id.new}    Update Dynamic Value    ${label.user_email_id}    ${email_ID}
    AppiumLibrary.Wait Until Element Is Visible    ${label.user_email_id.new}    ${LONG_WAIT}    User email is not visible

Logout from the app
    Swipe Down    2
    AppiumLibrary.Wait Until Element Is Visible    //android.view.View[@text='Logout of this app']    ${LONG_WAIT}
    AppiumLibrary.Click Element    //android.view.View[@text='Logout of this app']
    AppiumLibrary.Wait Until Element Is Visible    //android.widget.Button[@text='Yes']
    AppiumLibrary.Click Element    //android.widget.Button[@text='Yes']
    AppiumLibrary.Wait Until Element Is Visible    ${images.flipkartlogo}    ${LONG_WAIT}    Did not logout from application.

Enter Login Details
    [Arguments]    ${Username}    ${Password}
    Comment    CLick on login with email id.
    AppiumLibrary.Wait Until Element Is Visible    ${button.email_id}
    AppiumLibrary.Click Element    ${button.email_id}
    Comment    Enter user credentials
    Sleep    3s
    AppiumLibrary.Click Element    ${textbox.login.email}
    Input text    ${textbox.login.email}    ${Username}
    AppiumLibrary.Click Element    ${button.email.continue}
    sleep    3s
    Input text    com.flipkart.android:id/phone_input    ${Password}
    AppiumLibrary.Click Element    ${button.sign_in}
    sleep    5s
    AppiumLibrary.Wait Until Element Is Visible    ${images.flipkartlogo}    ${LONG_WAIT}

Print All Languages and Select Language
    [Arguments]    ${expected_langauge}
    FOR    ${value}    IN RANGE    1    3
        sleep    5s
        ${count}    Get Matching Xpath Count    ${label.welcome_page.languages}
        ${language}    Print Languages from App and Select Language    ${count}    ${expected_langauge}
        ${language}    convert to string    ${language}
        Exit for loop If    '${language}'=='অসমীয়া'
        Swipe Down     2
    END

Select Language
    [Arguments]    ${laguage_text}
    ${laguage_text}    Convert to string    ${laguage_text}
    ${list.language.new}    Update Dynamic Value    ${list.language}    ${laguage_text}
    AppiumLibrary.Wait Until Element Is Visible    ${list.language.new}    ${MEDIUM_WAIT}     Expected Language is not visible
    ${status}    Run Keyword And Return Status    Swipe Down To Element    ${list.language.new}    3
    Run keyword if    ${status}==True    AppiumLibrary.Click Element    ${list.language.new}
    AppiumLibrary.Element Should Be Visible    ${button.continue}
    AppiumLibrary.Click Element    ${button.continue}

Add new Address
    [Arguments]    ${name}    ${phone_number}    ${pincode}    ${house_no.}    ${area}
    Swipe down    2
    Sleep    3s
    AppiumLibrary.Click Element    ${button.edit.address}
    AppiumLibrary.Wait Until Element Is Visible    ${button.add.adress}
    AppiumLibrary.Click Element    ${button.add.adress}
    AppiumLibrary.Wait Until Element Is Visible    ${btton.pincode.alert.no}
    AppiumLibrary.Click Element    ${btton.pincode.alert.no}
    Input Text    //android.widget.EditText[@resource-id='name']    ${name}
    Input Text    //android.widget.EditText[@resource-id='phone']    ${phone_number}
    Input Text    //android.widget.EditText[@resource-id='pincode']    ${pincode}
    Input Text    //android.widget.EditText[@resource-id='addressLine1']    ${house_no.}
    Input Text    //android.widget.EditText[@resource-id='addressLine2']    ${area}
    AppiumLibrary.Click Element    //android.view.View[@text='Home']
    AppiumLibrary.Click Element    //android.widget.Button[@text='Save Address']

Select Edit Adress
    Swipe down    2
    sleep    3s
    AppiumLibrary.Click Element    ${button.edit.address}
    AppiumLibrary.Wait Until Element Is Visible    ${button.add.adress}
