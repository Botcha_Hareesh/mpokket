*** Settings ***
Resource          ../../Config/super.robot

*** Test Cases ***
TC01 - Launch Flipkart app
    Comment    Launch the Flipkart app
    mobile_common.Launch Mobile Application    ${PLATFORM_NAME}
    [Teardown]    AppiumLibrary.Close All Applications

TC02 - Print the languages and select language as English
    Comment    Launch the Flipkart app
    mobile_common.Launch Mobile Application    ${PLATFORM_NAME}
    Comment    Print Languages from App and Select Language
    Print All Languages and Select Language    English

TC03 - Login with email id and password
    [Setup]    Read TestData From Excel    TC_01    Login
    Comment    Launch the Flipkart app
    mobile_common.Launch Mobile Application    ${PLATFORM_NAME}
    Comment    Select Language
    Select Language    English
    Comment    Login to Flipkart application
    Login to app    ${test_prerequisite_data}[Username]    ${test_prerequisite_data}[Password]

TC04 - Click on Menu and click MyAccount
    [Tags]    Android_test_case_id=85483
    [Setup]    Read TestData From Excel    TC_01    Login
    Comment    Launch the Flipkart app
    mobile_common.Launch Mobile Application    ${PLATFORM_NAME}
    Comment    Select Language
    Select Language    English
    Comment    Login to Flipkart application
    Login to app    ${test_prerequisite_data}[Username]    ${test_prerequisite_data}[Password]
    Comment    Navigate to MyAccount
    Navigate to MyAccount    ${test_prerequisite_data}[Username]

TC05 - Add a new Address
    [Setup]    Read TestData From Excel    TC_01    Login
    Comment    Launch the Flipkart app
    mobile_common.Launch Mobile Application    ${PLATFORM_NAME}
    Comment    Select Language
    Select Language    English
    Comment    Login to Flipkart application
    Login to app    ${test_prerequisite_data}[Username]    ${test_prerequisite_data}[Password]
    Comment    Navigate to MyAccount
    Navigate to MyAccount    ${test_prerequisite_data}[Username]
    Comment    Navigate to MyAccount
    Add new Address    ${test_prerequisite_data}[Name]    ${test_prerequisite_data}[Phoneno]    ${test_prerequisite_data}[Pincode]    ${test_prerequisite_data}[HouseNo]    ${test_prerequisite_data}[Area]

TC06 - Click on Edit Profile
    [Setup]    Read TestData From Excel    TC_01    Login
    Comment    Launch the Flipkart app
    mobile_common.Launch Mobile Application    ${PLATFORM_NAME}
    Comment    Select Language
    Select Language    English
    Comment    Login to Flipkart application
    Login to app    ${test_prerequisite_data}[Username]    ${test_prerequisite_data}[Password]
    Comment    Navigate to MyAccount
    Navigate to MyAccount    ${test_prerequisite_data}[Username]
    Comment    Navigate to MyAccount
    Select Edit Adress

TC07 - Logout from App
    [Setup]    Read TestData From Excel    TC_01    Login
    Comment    Launch the Flipkart app
    mobile_common.Launch Mobile Application    ${PLATFORM_NAME}
    Comment    Select Language
    Select Language    English
    Comment    Login to Flipkart application
    Login to app    ${test_prerequisite_data}[Username]    ${test_prerequisite_data}[Password]
    Comment    Navigate to MyAccount
    Navigate to MyAccount    ${test_prerequisite_data}[Username]
    Comment    Logout from the app
    Logout from the app
