*** Variables ***
${images.welcome.logo}    //android.widget.ImageView[@index='0']
${images.enrollees.search_icon}    //android.view.View[@text='Enrollees']/preceding::android.widget.ImageView[1]
${images.enrollees.plus_icon}    //android.view.View[@text='Enrollees']/following::android.widget.ImageView[1]
${image.enrollee.profilephoto}    (//android.widget.ScrollView[@index='0']//following-sibling::android.view.ViewGroup[@index='0'])[4]
${image.enrollee.profilephoto.captureimage}    //android.widget.ImageView[@content-desc="Shutter"]
${image.enrolee.profilephoto.capture.okimage}    //android.widget.ImageButton[@content-desc="Done"]
${images.enrollee.edit.pursue}    //android.widget.TextView[@text='pursue']/preceding::android.widget.ImageView[1]
${images.enrollee.pursue}    //android.widget.TextView[@text='pursue']/preceding::android.widget.ImageView[1]
${images.dashboard.caseload.enrollees}    //android.view.View[@content-desc='Google Map']/android.view.View
${images.dashboard.caseload.google_map}    //android.view.View[@content-desc='Google Map']
${images.dashboard.recent_pursue_enrollee}    //android.widget.HorizontalScrollView//android.widget.ImageView[1]
