*** Variables ***
${list.addenrollee.list_values}    //android.widget.ScrollView[@index='0']
${list.enrollee.profiledts}    //android.widget.TextView[contains(@text,'replaceText')]
${list.schema}    //android.widget.TextView[@text='replaceText']
${list.enrollee.org}    //android.widget.TextView[contains(@text,'replaceText')]
${common.dynamic_xpath}    //android.widget.TextView[contains(@text,'replaceText')]
${list.google_map.enrollee.count}    //android.view.View[@content-desc='Google Map']/android.view.View[replaceText]
${list.enrollee.pursuit.primary_id}    //android.widget.TextView[contains(@text,'replaceText')]/preceding-sibling::android.view.ViewGroup[1]
@{testdata_list}    LastName    PrimaryId    Organization    RiskLevel    TimeZone
${list.assign_device}    //android.widget.TextView[contains(@text,'replaceText')]
${list.events.open_events}    //android.widget.TextView[contains(@text,'replaceText')]
${list.events}    //android.widget.TextView[@text='replaceText1']//following-sibling::android.widget.TextView[contains(@text,'replaceText2')]
