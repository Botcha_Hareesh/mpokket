*** Variables ***
${TESTDATA_FOLDER}    ${EXECDIR}/Testdata
${LONG_WAIT}      50s
${MEDIUM_WAIT}    10s
${SHORT_WAIT}     5s
${PLATFORM_NAME}    Android    # Args can be : Android or iOS
${FULL_NAME}      ${EMPTY}
${primary_id}     ${EMPTY}
${organization}    ${EMPTY}
@{enrollee_main_list}
${device_id}      ${EMPTY}
${language}       ${EMPTY}
